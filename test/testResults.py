import unittest

import logchain


class TestChaining(unittest.TestCase):

	def test_positive_negative(self):
		self.assertTrue(logchain.Result(True))
		self.assertFalse(logchain.Result(False))

	def test_extra_params(self):
		r = logchain.Result(False, a = 1, b = "hello")
		self.assertFalse(r)
		self.assertEqual(r.a, 1)
		self.assertEqual(r.b, "hello")
