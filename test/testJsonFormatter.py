import unittest
import secrets
import logging
import json
import io

import logchain


class TestJsonFormatter(unittest.TestCase):

	def tearDown(self):
		logchain.stopLogging()

	def test_ctor(self):
		"""
		Various initialization scenarii.
		"""
		# Right default values
		chainer = logchain.LogChainer(formatterCls = logchain.formatters.Json)
		fields = chainer.formatter.fields
		self.assertIn("msg", fields)
		self.assertIn("timestamp", fields)
		self.assertIn("signature", fields)
		self.assertNotIn("custom", fields)

		# Extra fields
		chainer = logchain.LogChainer(formatterCls = logchain.formatters.Json,
		                                 extraFields  = {"custom"})
		fields = chainer.formatter.fields
		self.assertIn("msg", fields)
		self.assertIn("custom", fields)

		# Fine tuning fields
		chainer = logchain.LogChainer(formatterCls = logchain.formatters.Json,
		                                fields       = {"a", "b"},
										extraFields  = {"c"})
		fields = chainer.formatter.fields
		self.assertEqual(fields, {"a", "b", "c"})


	def test_content(self):
		# Grab the logging output
		logStream = io.StringIO()
		chainer = logchain.LogChainer(stream = logStream,
		                                verbosity = 5,
		                                formatterCls = logchain.formatters.Json,
		                                secret = "Et2FwQefvb7HfCb7tATguSicVj_7TVlM")
		chainer.initLogging()

		# Some varied content
		logging.debug("Hello debug இ éïô $*µ$,;:!§/.?<>|ç&[ 3² + 4² = 5² ]")
		logging.info("this is useful info")
		logging.warning("warning!")
		logging.error("error message")
		logging.critical("Good bye.")

		lines = logStream.getvalue().strip().split("\n")
		self.assertEqual(len(lines), 5)

		firstLine = json.loads(lines[0])

		self.assertTrue({"signature", "timestamp", "fileLine", "levelno", "msg", "process", "thread"} <= firstLine.keys())
		self.assertEqual(firstLine["msg"], "Hello debug இ éïô $*µ$,;:!§/.?<>|ç&[ 3² + 4² = 5² ]")


	def test_verify(self):
		chainer = logchain.LogChainer(formatterCls = logchain.formatters.Json,
		                                secret = "Et2FwQefvb7HfCb7tATguSicVj_7TVlM")
		lines = [
			'{"fileLine": "TestJsonFormatter.py:48", "levelno": 10, "msg": "Hello debug இ éïô $*µ$,;:!§/.?<>|ç&[ 3² + 4² = 5² ]", "process": 13615, "processName": "MainProcess", "signature": "67c2369cb5386541", "thread": 140242165983040, "threadName": "MainThread", "timestamp": "2020-04-26 13:56:32.048"}',
			'{"fileLine": "TestJsonFormatter.py:49", "levelno": 20, "msg": "this is useful info", "process": 13615, "processName": "MainProcess", "signature": "d7b8f8da61f214ee", "thread": 140242165983040, "threadName": "MainThread", "timestamp": "2020-04-26 13:56:32.048"}',
			'{"fileLine": "TestJsonFormatter.py:50", "levelno": 30, "msg": "warning!", "process": 13615, "processName": "MainProcess", "signature": "795ddd316dfcf908", "thread": 140242165983040, "threadName": "MainThread", "timestamp": "2020-04-26 13:56:32.048"}',
			'{"fileLine": "TestJsonFormatter.py:51", "levelno": 40, "msg": "error message", "process": 13615, "processName": "MainProcess", "signature": "93ade9e6e134639b", "thread": 140242165983040, "threadName": "MainThread", "timestamp": "2020-04-26 13:56:32.049"}',
			'{"fileLine": "TestJsonFormatter.py:52", "levelno": 50, "msg": "Good bye.", "process": 13615, "processName": "MainProcess", "signature": "ade925c004dca5dd", "thread": 140242165983040, "threadName": "MainThread", "timestamp": "2020-04-26 13:56:32.049"}'
		]

		verif = chainer.verify(lines)
		self.assertTrue(verif)

		# Deletion
		lines = [
			'{"fileLine": "TestJsonFormatter.py:49", "levelno": 20, "msg": "this is useful info", "process": 13615, "processName": "MainProcess", "signature": "d7b8f8da61f214ee", "thread": 140242165983040, "threadName": "MainThread", "timestamp": "2020-04-26 13:56:32.048"}',
			'{"fileLine": "TestJsonFormatter.py:52", "levelno": 50, "msg": "Good bye.", "process": 13615, "processName": "MainProcess", "signature": "ade925c004dca5dd", "thread": 140242165983040, "threadName": "MainThread", "timestamp": "2020-04-26 13:56:32.049"}'
		]
		verif = chainer.verify(lines)
		self.assertFalse(verif)

		# Tampering
		lines = [
			'{"fileLine": "FakeFile.py:49", "levelno": 20, "msg": "this is useful info", "process": 13615, "processName": "MainProcess", "signature": "d7b8f8da61f214ee", "thread": 140242165983040, "threadName": "MainThread", "timestamp": "2020-04-26 13:56:32.048"}',
			'{"fileLine": "TestJsonFormatter.py:50", "levelno": 30, "msg": "warning!", "process": 13615, "processName": "MainProcess", "signature": "795ddd316dfcf908", "thread": 140242165983040, "threadName": "MainThread", "timestamp": "2020-04-26 13:56:32.048"}',
		]
		verif = chainer.verify(lines)
		self.assertFalse(verif)


	def test_update_context(self):

		# Concrete example: an App handling transactions
		# Set some contextual information
		class App:
			def __init__(self, appName, logger):
				self.logger = logger
				self.logger.setFields(appName = appName)
				logging.info("Creating the app")

			def handleTransaction(self, userId, callback):
				with self.logger.managedFields(uId = userId, trxId = secrets.token_urlsafe(8)):
					callback()

			def close(self):
				logging.info("Closing the app")


		# The log chain in transparent for the callbacks
		def callback1():
			logging.warning("Something happened")

		def callback2():
			logging.info("Serving a resource")

		# Grab the logging output
		logStream = io.StringIO()
		params = {
			"stream": logStream,
			"verbosity": 5,
			"formatterCls": logchain.formatters.Json,
			"fields": {"signature", "msg"}
		}

		chainer = logchain.LogChainer(**params)
		chainer.initLogging()

		app = App("MyApp", chainer)
		app.handleTransaction("user1", callback1)
		app.handleTransaction("user1", callback2)
		app.close()

		#print(logStream.getvalue())
		lines = logStream.getvalue().strip().split("\n")
		self.assertEqual(len(lines), 4)

		verif = chainer.verify(lines)
		self.assertTrue(verif)

		line0 = json.loads(lines[0])
		self.assertTrue({"appName", "msg"} <= line0.keys())
		line1 = json.loads(lines[1])
		self.assertTrue({"appName", "msg", "trxId", "uId"} <= line1.keys())
		line2 = json.loads(lines[2])
		self.assertTrue({"appName", "msg", "trxId", "uId"} <= line2.keys())
		self.assertNotEqual(line1["trxId"], line2["trxId"])
		line3 = json.loads(lines[3])
		self.assertFalse({"trxId", "uId"} <= line3.keys())
