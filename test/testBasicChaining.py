"""
Default usage.
"""
import unittest
import logging
import time
import io
import os
from types import SimpleNamespace

import logchain


class TestBasicChaining(unittest.TestCase):

	@classmethod
	def tearDownClass(cls):
		logchain.stopLogging()

	def test_ctor(self):
		# Init by named arguments.
		chainer = logchain.LogChainer(verbosity = 5)

		self.assertTrue(hasattr(chainer, "formatter"))
		formatter = chainer.formatter

		# Strong default values
		self.assertTrue(hasattr(formatter, "secret"))
		self.assertGreater(len(formatter.secret), 100)

		self.assertTrue(hasattr(formatter, "prevLine"))
		self.assertGreater(len(formatter.prevLine), 30)

		# Right custom values
		self.assertEqual(chainer.verbosity, 5)


	def test_logging_happy_case(self):
		"""
		Regular init then usage,
		we verify:
		- the logs are well generated
		- the chain is consistent
		"""

		# Grab the logging output
		logStream = io.StringIO()

		chainer = logchain.LogChainer(stream = logStream, verbosity = 5)
		chainer.initLogging()

		logging.debug("hello gg")
		logging.info("voila1")
		logging.warning("voila2")
		logging.error("voila3")
		logging.critical("voila4")

		lines = logStream.getvalue().strip().split("\n")
		self.assertEqual(len(lines), 5)
		#print(chainer.secret)
		#print(lines)

		verif = chainer.verify(lines)
		self.assertTrue(verif)


	def test_verify_happy_case(self):
		"""
		Focus on the consistency check of a predefined chain
		"""

		aLogChain = [
			'2020-04-25 00:21:36.266 TestChaining.py:33 test_logging_happy_case hello gg |862101dead44e3fb',
			'2020-04-25 00:21:36.266 TestChaining.py:34 test_logging_happy_case voila1 |87f4b398040df0b0',
			'2020-04-25 00:21:36.266 TestChaining.py:35 test_logging_happy_case voila2 |98e33f86376f5858',
			'2020-04-25 00:21:36.267 TestChaining.py:36 test_logging_happy_case voila3 |0757cf00b412a767',
			'2020-04-25 00:21:36.267 TestChaining.py:37 test_logging_happy_case voila4 |95ea9b92cc3e1bc7'
		]

		chainer = logchain.LogChainer(secret = "Et2FwQefvb7HfCb7tATguSicVj_7TVlM", name = "Gg")
		verif = chainer.verify(aLogChain)
		self.assertTrue(verif)


	def test_checking_missing_line(self):
		"""
		Spot the inconsistency in a log chain where one line is deleted
		"""

		# One line deleted
		aLogChain = [
			'2020-04-25 00:21:36.266 TestChaining.py:33 test_logging_happy_case hello gg |862101dead44e3fb',
			'2020-04-25 00:21:36.266 TestChaining.py:34 test_logging_happy_case voila1 |87f4b398040df0b0',
			'2020-04-25 00:21:36.267 TestChaining.py:36 test_logging_happy_case voila3 |0757cf00b412a767',
			'2020-04-25 00:21:36.267 TestChaining.py:37 test_logging_happy_case voila4 |95ea9b92cc3e1bc7'
		]
		chainer = logchain.LogChainer(secret = "Et2FwQefvb7HfCb7tATguSicVj_7TVlM")
		verif = chainer.verify(aLogChain)
		self.assertFalse(verif)
		self.assertEqual(verif.prevLine, aLogChain[1])
		self.assertEqual(verif.line,     aLogChain[2])
		#print("Inconsistency between lines:\nOK> %s\nKO> %s" % verif[1:])


	def test_checking_wrong_signature(self):
		"""
		Spot the inconsistency in a log chain where one line is modified
		"""

		# One line modified
		aLogChain = [
			'2020-04-25 00:21:36.266 TestChaining.py:33 test_logging_happy_case hello gg |862101dead44e3fb',
			'2020-04-25 00:21:36.266 TestChaining.py:34 test_logging_happy_case voila1 |87f4b398040df0b0',
			'2020-04-25 00:21:36.266 TestChaining.py:35 test_logging_happy_case voila2 |98e33f86376f5858',
			'2020-04-25 00:21:36.267 TestChaining.py:36 test_logging_happy_case voila5 |0757cf00b412a767', # altered
			'2020-04-25 00:21:36.267 TestChaining.py:37 test_logging_happy_case voila4 |95ea9b92cc3e1bc7'
		]
		chainer = logchain.LogChainer(secret = "Et2FwQefvb7HfCb7tATguSicVj_7TVlM")
		verif = chainer.verify(aLogChain)
		self.assertFalse(verif)
		self.assertEqual(verif.prevLine, aLogChain[3])
		self.assertEqual(verif.line,     aLogChain[4])
		#print("Inconsistency between lines:\nOK> %s\nKO> %s" % verif[1:])


	def test_timestamp(self):
		baseParams = { "secret": "", "seed": "" }

		os.environ["TZ"] = "Europe/Paris"
		time.tzset()
		record = SimpleNamespace(created = 1585964800)

		# Iso, local, milliseconds by default
		aFormatter = logchain.formatters.Basic(baseParams)
		ts = aFormatter.makeTimestamp(record)
		self.assertEqual(ts, "2020-04-04 03:46:40.000")

		# To UTC (depends on current timezone)
		params = {**baseParams, "timestamp": {"utc": True}}
		aFormatter = logchain.formatters.Basic(params)
		ts = aFormatter.makeTimestamp(record)
		self.assertEqual(ts, "2020-04-04 01:46:40.000")

		# End with %f, truncate to millis
		params = {**baseParams, "timestamp": {"fmt": "%d %T.%f", "precision": "milliseconds"} }
		aFormatter = logchain.formatters.Basic(params)
		ts = aFormatter.makeTimestamp(record)
		self.assertEqual(ts, "04 03:46:40.000")

		# Microseconds
		params = {**baseParams, "timestamp": {"fmt": "%d %T.%f", "precision": "microseconds"} }
		aFormatter = logchain.formatters.Basic(params)
		ts = aFormatter.makeTimestamp(record)
		self.assertEqual(ts, "04 03:46:40.000000")

		# Not truncated
		params = {**baseParams, "timestamp": {"fmt": "%F %T.%f gg", "precision": "milliseconds"} }
		aFormatter = logchain.formatters.Basic(params)
		ts = aFormatter.makeTimestamp(record)
		self.assertEqual(ts, "2020-04-04 03:46:40.000000 gg")


	def test_named_logger(self):
		logStream = io.StringIO()

		chainer = logchain.LogChainer(name = "martin", stream = logStream, verbosity = 5)
		logger1 = chainer.initLogging()

		logger1.debug("hello1")

		logger2 = logging.getLogger("martin")
		logger2.debug("hello2")

		# Both loggers references point to the right instance.
		lines = logStream.getvalue().strip().split("\n")
		self.assertEqual(len(lines), 2)

		verif = chainer.verify(lines)
		self.assertTrue(verif)
